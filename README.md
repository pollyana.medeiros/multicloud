Projeto de frontend em Django para a biblioteca Apache Libcloud([Libcloud](https://libcloud.readthedocs.io)) que permite administrar nuvens públicas. 

Esta biblioteca Python que esconde diferenças entre 
diferentes APIs de provedores de nuvem e permite que você gerencie diferentes recursos de nuvem por meio de uma API unificada e fácil de usar. Os recursos que você pode gerenciar com o Libcloud são divididos nas seguintes categorias:

* Cloud Servers e Block Storage
* Cloud Object Storage e CDN
* Load Balancers as a Service
* DNS como um serviço
* Container Services
* Backup como um serviço

![Menus](/diagramas/multi-clod-diagram.png)


**Instalação do multi-cloud**


**1)Instalar o Python 4**
<pre>
#apt-get install python3
</pre>
**2)Instalar o Pip**
<pre>
#apt-get install pip
</pre>

**3)Instalar as dependências**
<pre>
#pip install apache-libcloud

#pip install mysqlclient

#pip install bootstrap-admin

#pip install django

#pip install django-crispy-forms

#pip install django-rest

#pip install django-extensions
</pre>
**4)Criar base no mysql conforme backup no diretório BD e ajustar o settings.py conforme abaixo : **
<pre>

DATABASES = {

    'default': {
    
        'ENGINE': 'django.db.backends.mysql',
        
        'NAME': 'nome-do-banco',
        
        'USER': 'usuario',
        
        'PASSWORD': 'senha',
        
        'HOST': 'endereço-IP',   # Or an IP Address that your DB is hosted on
        
        'PORT': '3306',
        
    }
    
}
</pre>
**5)No diretório com os códigos criados** 
<pre>
#python manage.py migrate

#python manage.py createsuperuser

#python manage.py runserver
</pre>