from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver


AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

volumeID = ''
instanceID = ''
device = '/dev/xvdb'

instances = driver.list_nodes()
#print(instances)

volumes = driver.list_volumes()
#print(volumes)


for instance in instances:
    if instance.id == instanceID:
        print(instance)
        for volume in volumes:
            if volume.id == volumeID:
                print(volume)
                attach = driver.attach_volume(instance, volume, device)
                print(attach)