from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver


AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

volumeID = 'vol-08e38e433be67ffe4'
instanceID = 'i-0581f78effbfa6773'
device = '/dev/xvdb'

instances = driver.list_nodes()
#print(instances)

volumes = driver.list_volumes()
#print(volumes)

for volume in volumes:
    if volume.id == volumeID:
        print(volume)
        desattach = driver.detach_volume(volume)
        print(desattach)