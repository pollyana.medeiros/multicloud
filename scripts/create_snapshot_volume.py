from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver


AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

volumeID = 'vol-015371ddd413f1756'
instanceID = 'i-0581f78effbfa6773'

instances = driver.list_nodes()
print(instances)

volumes = driver.list_volumes()
print(volumes)

for volume in volumes:
    if volume.id == volumeID:
        print(volume)
        snapshot = driver.create_volume_snapshot
        print(snapshot)

