from django.db import models as models
from django.urls import reverse
from django.dispatch import receiver
from simple_history.models import HistoricalRecords

import signal


class CREDENTIALS(models.Model):

# Fields
    id = models.CharField(max_length=60, primary_key=True)
    chave = models.CharField(max_length=60)
#inclusao nome
    nome =  models.CharField(max_length=60)

    class Meta:
        verbose_name_plural = "Credenciais"
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('amazon_credentials_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('amazon_credentials_update', args=(self.pk,))

    def __str__(self):              # __unicode__ on Python 2
        return self.nome

class KEYS(models.Model):
    # Fields
    NAME = models.CharField(max_length=60)
    FINGERPRINT = models.CharField(max_length=60)

    # Relationship Fields
    AWS_EC2_ACCESS_ID = models.ForeignKey(
        'amazon.CREDENTIALS',
        on_delete=models.CASCADE, related_name="keys",
    )

    class Meta:
        verbose_name_plural = "Keys"
        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.id

    def get_absolute_url(self):
        return reverse('amazon_keys_detail', args=(self.id,))

    def get_update_url(self):
        return reverse('amazon_keys_update', args=(self.id,))

    def __str__(self):              # __unicode__ on Python 2
        return self.NAME

class VOLUMES(models.Model):
    SIZE_ID = (
        ('' , 'Escolha...') ,
        ('10' , '10 GB'), ('20' , '20 GB'),('30' , '30 GB'), ('40' , '40 GB'),('50' , '50 GB'), ('60' , '60 GB'),('70' , '70 GB'), ('80' , '80 GB'),('90' , '90 GB'), ('100' , '100 GB')
    )
    DEV = (
        ('','Escolha...'),
        ('/dev/sdf','/dev/sdf'),
        ('/dev/xvx2','/dev/xvx2'),
    )
    # Fields
    VOLUMEID = models.CharField(max_length=20)
    DEVICE = models.CharField(max_length=20, choices=DEV)
    SIZE = models.CharField(max_length=20, choices=SIZE_ID)
    AWS_REGION = models.CharField(max_length=20)
    NAME = models.CharField(max_length=20)
    TYPE = models.CharField(max_length=20)
    SNAPSHOT = models.CharField(max_length=20)
    CREATE_DATE = models.CharField(max_length=20)
    STATUS = models.CharField(max_length=20)
    IOPS = models.CharField(max_length=20)
    AWS_EC2_ACCESS_ID_id = models.CharField(max_length=20)

#    AWS_EC2_ACCESS_ID = models.ForeignKey(
#        'amazon.CREDENTIALS',
#        on_delete=models.CASCADE, related_name="volumes",
#    )

    NODE_ID = models.ForeignKey(
        'amazon.INSTANCE',
        on_delete=models.CASCADE, related_name="instanciasVol",
    )

    class Meta:
        verbose_name_plural = "Volumes"
        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.id

    def get_absolute_url(self):
        return reverse('amazon_volumes_detail', args=(self.id,))

    def get_update_url(self):
        return reverse('amazon_volumes_update', args=(self.id,))

    def __str__(self):              # __unicode__ on Python 2
        return self.VOLUMEID

class INSTANCE(models.Model):
    REGION = (
        ('' , 'Escolha...') ,
        ('us-east-1a' , 'Leste dos EUA 1a') ,
        ('us-east-1b' , 'Leste dos EUA 1b') ,
        ('us-east-1c' , 'Leste dos EUA 1c')
    )
    TYPES = (
        ('' , 'Escolha...') ,
        ('m1.small', 'm1.small'),
        ('m1.medium', 'm1.medium'),
        ('m1.large', 'm1.medium')
    )
    AMI_ID = (
        ('' , 'Escolha...') ,
         ('ami-013e2b5127d181b0c', 'Windows 2016 Server'),
        ('ami-0cfee17793b08a293', 'Ubuntu Server 16.04 LTS (HVM), SSD Volume Type')
    )

    # Fields
    AWS_REGION = models.CharField(max_length=10 , choices=REGION)
    SIZE_ID = models.CharField(max_length=10 , choices=TYPES)
    AMI_ID = models.CharField(max_length=30, choices=AMI_ID)
    NAME = models.CharField(max_length=30)
    NODE_STATE_MAP = models.CharField(max_length=30)
    NODE_ID = models.CharField(max_length=30)
    IP = models.CharField(max_length=30)
    OWNER = models.CharField(max_length=30)
    LAUCH_TIME =  models.CharField(max_length=30)
    LAST_REBOOT =  models.CharField(max_length=30)
#   history = HistoricalRecords()

    # Relationship Fields
    AWS_EC2_ACCESS_ID = models.ForeignKey(
        'amazon.CREDENTIALS',
        on_delete=models.CASCADE, related_name="instances",
    )
    KEY_ID = models.ForeignKey(
        'amazon.KEYS',
        on_delete=models.CASCADE, related_name="keys_id",
    )

    class Meta:
        verbose_name_plural = "Instâncias"
        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.id

    def get_absolute_url(self):
        return reverse('amazon_instance_detail', args=(self.id,))

    def get_update_url(self):
        return reverse('amazon_instance_update', args=(self.id,))

    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def __str__(self):              # __unicode__ on Python 2
        return self.NODE_ID


class IMAGES(models.Model):
    # Fields
    NAME = models.CharField(max_length=60)
    IMAGE_ID = models.CharField(max_length=60)

# Relationship Fields
#    AWS_EC2_ACCESS_ID = models.ForeignKey(
#        'amazon.CREDENTIALS',
#        on_delete=models.CASCADE, related_name="images",
#    )

    NODE_ID = models.ForeignKey(
        'amazon.INSTANCE',
        on_delete=models.CASCADE, related_name="images",
    )

    class Meta:
        verbose_name_plural = "images"
        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.id

    def get_absolute_url(self):
        return reverse('amazon_images_detail', args=(self.id,))

    def get_update_url(self):
        return reverse('amazon_images_update', args=(self.id,))


class SNAPSHOTS(models.Model):
    # Fields
    NAME = models.CharField(max_length=60)
    SNAPSHOT_ID = models.CharField(max_length=60)

    # Relationship Fields

    NODE_ID = models.ForeignKey(
        'amazon.VOLUMES',
        on_delete=models.CASCADE, related_name="snapshots",
    )

    class Meta:
        verbose_name_plural = "snapshots"
        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.id

    def get_absolute_url(self):
        return reverse('amazon_snapshots_detail', args=(self.id,))

    def get_update_url(self):
        return reverse('amazon_snapshots_update', args=(self.id,))
