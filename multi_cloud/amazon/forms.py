from django import forms
from .models import CREDENTIALS, INSTANCE, VOLUMES, KEYS, IMAGES, SNAPSHOTS

class CREDENTIALSForm(forms.ModelForm):
    class Meta:
        model = CREDENTIALS
        fields = ['id', 'chave', 'nome']

class INSTANCEForm(forms.ModelForm):
    class Meta:
        model = INSTANCE
        fields = ['AWS_REGION', 'AMI_ID', 'SIZE_ID', 'NAME', 'NODE_STATE_MAP', 'IP', 'AWS_EC2_ACCESS_ID']

class VOLUMESForm(forms.ModelForm):
    class Meta:
        model = VOLUMES
        fields = ['VOLUMEID', 'SIZE', 'AWS_REGION', 'NAME', 'TYPE', 'CREATE_DATE', 'STATUS']

class KEYSForm(forms.ModelForm):
    class Meta:
        model = KEYS
        fields = ['NAME', 'FINGERPRINT']

class IMAGESForm(forms.ModelForm):
    class Meta:
        model = IMAGES
        fields = ['NAME', 'IMAGE_ID']

class SNAPSHOTSForm(forms.ModelForm):
    class Meta:
        model = SNAPSHOTS
        fields = ['NAME', 'SNAPSHOT_ID']